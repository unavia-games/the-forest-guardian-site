import "typeface-montserrat";
import { format, parseISO } from "date-fns";

// Components
import Hover from "@components/Hover";
import MdiIcon from "@components/MdiIcon";
import Modal from "@components/Layout/Modal";
import PageSection from "@components/Layout/PageSection";
import DefaultLayout from "@layouts/Default";

// Utilities
import { breakpointMixin } from "@utilities/breakpoints";
import { dateFilter } from "@utilities/filters";

// Styles
import "@styles/styles.scss";

export default function (Vue) {
  Vue.component("Hover", Hover);
  // Set default layout as a global component
  Vue.component("Layout", DefaultLayout);
  Vue.component("Modal", Modal);
  Vue.component("PageSection", PageSection);
  Vue.component("MdiIcon", MdiIcon);

  Vue.mixin(breakpointMixin);

  Vue.filter("formatDate", dateFilter);

  Vue.prototype.$formatDate = format;
  Vue.prototype.$parseISO = parseISO;
}
