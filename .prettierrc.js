module.exports = {
  endOfLine: "lf",
  trailingComma: "es5",
  singleQuote: false,
  semi: true,
};
