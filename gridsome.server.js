// Utilities
const config = require("./config.js");

// NOTE: Changes here require a server restart.

module.exports = function (api) {
  api.loadSource(({ addMetadata }) => {
    addMetadata("repositoryUrl", config.repositoryUrl);
  });
};
