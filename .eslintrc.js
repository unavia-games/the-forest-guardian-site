module.exports = {
  env: {
    es6: true,
    node: true,
  },
  parserOptions: {
    ecmaVersion: 11,
    sourceType: "module",
  },
  parser: "vue-eslint-parser",
  plugins: ["gridsome"],
  extends: [
    "eslint:recommended",
    "plugin:vue/recommended",
    "plugin:prettier/recommended",
  ],
  rules: {
    "gridsome/format-query-block": "off",
    "no-unused-vars": "warn",
    "prettier/prettier": "warn",
    "vue/attribute-hyphenation": "off",
    "vue/html-self-closing": "off",
    "vue/max-attributes-per-line": "off",
    "vue/singleline-html-element-content-newline": "off",
  },
};
