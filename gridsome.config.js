const merge = require("webpack-merge");
const path = require("path");

const Paths = {
  src: path.join(__dirname, "src"),
};

const siteName = "The Forest Guardian";

// Provide the "_vars" SCSS file to all components
// Taken from: https://gridsome.org/docs/assets-css/#global-preprocessor-files-ie-variables-mixins
const globalScssFiles = ["_vars", "_breakpoints", "_mixins"];
const globalScssPaths = globalScssFiles.map((f) =>
  path.resolve(`./src/styles/${f}.scss`)
);
function addStyleResource(rule) {
  rule.use("style-resource").loader("style-resources-loader").options({
    patterns: globalScssPaths,
  });
}

module.exports = {
  icon: "./src/favicon.png",
  siteName,
  siteUrl: "https://www.theforestguardian.com",
  titleTemplate: "%s - The Forest Guardian",

  plugins: [
    {
      use: "gridsome-plugin-pwa",
      options: {
        title: siteName,
        shortName: "Forest Guardian",
        categories: ["game", "unity"],
        lang: "en-US",
        //startUrl: "/",
        //display: "standalone",
        //statusBarStyle: "default",
        disableServiceWorker: false,
        //cachedFileTypes: "js,json,css,html,png,jpg,jpeg,svg",
        themeColor: "#a6d388",
        // themeColor: "#77b255",
        backgroundColor: "#e6e0dd",
        // Must be provided like "src/favicon.png"
        icon: "src/favicon.png",
        maskableIcon: true,
      },
    },
  ],

  chainWebpack(config) {
    const types = ["vue-modules", "vue", "normal-modules", "normal"];

    types.forEach((type) => {
      addStyleResource(config.module.rule("scss").oneOf(type));
      addStyleResource(config.module.rule("sass").oneOf(type));
    });
  },

  configureWebpack(config) {
    return merge(
      {
        resolve: {
          alias: {
            "@": Paths.src,
            "@assets": path.join(Paths.src, "assets"),
            "@config": path.join(Paths.src, "../config.js"),
            "@components": path.join(Paths.src, "components"),
            "@layouts": path.join(Paths.src, "layouts"),
            "@pages": path.join(Paths.src, "pages"),
            "@styles": path.join(Paths.src, "styles"),
            "@templates": path.join(Paths.src, "templates"),
            "@utilities": path.join(Paths.src, "utilities"),
          },
        },
      },
      config
    );
  },
};
